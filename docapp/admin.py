from django.contrib import admin
from .models import DocumentStatus, Document,Employee
from .models import Keywords, DocumentRoles, DocumentNumberStatus
from .models import DocumentType, Project, AuditLog
from .models import DocumentNumber, DocNumberSequence, DocRoleMapping
from .models import DocKeywordMapping, DocumentActivity

admin.site.register(DocumentStatus)
admin.site.register(Document)
admin.site.register(Employee)
admin.site.register(Keywords)
admin.site.register(DocumentRoles)
admin.site.register(DocumentNumberStatus)
admin.site.register(DocumentType)
admin.site.register(Project)
admin.site.register(AuditLog)
admin.site.register(DocumentNumber)
admin.site.register(DocNumberSequence)
admin.site.register(DocRoleMapping)
admin.site.register(DocKeywordMapping)
admin.site.register(DocumentActivity)
