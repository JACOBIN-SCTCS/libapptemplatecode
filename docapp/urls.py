from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name='docapp'
urlpatterns = [
    path("", views.index, name="index"),
    path("newdoc",views.newdoc, name="newdoc"),


    ############ URLS for page showing the list of Documents #############

    path("doclist/<str:person_type>",views.doclist,name="doc_list"),
    path("doclist/<str:person_type>/filtered", views.doclist_filtered, name="doc_list_filtered"),

    #######################################################################

    ############ URLS for page showing the details of a Document #############
    path("docdetails/<int:doc_id>", views.docdetails, name="docdetails"),
    
    # URLS for editing/adding details of a document

    ### BASIC DETAILS ##############
    path("doceditbasic/<int:doc_id>", views.doceditbasic, name="doceditbasic"),
    path("docgetbasic/<int:doc_id>", views.docgetbasic, name="docgetbasic"),
    ###############################

    ### KEYWORDS ##################
    path("doceditkeywords/<int:doc_id>",  views.doceditkeywords, name="doceditkeywords"),
    path("docgetkeywords/<int:doc_id>", views.docgetkeywords, name="docgetkeywords"),
    ################################

    ### ENTITY_REPORT_TYPE #######
    path("doceditprojectreporttype/<int:doc_id>", views.doceditentityreporttype, name="doceditentityreporttype"),
    path("docgetprojectreporttype/<int:doc_id>", views.docgetentityreporttype, name="docgetentityreporttype"),
    ###############################

    ### PERSON TYPE ##############
    path("doceditpersontype/<int:doc_id>/<str:person_type>", views.doceditpersontype, name="doceditpersontype"),
    path("docgetpersontype/<int:doc_id>/<str:person_type>", views.docgetpersontype, name="docgetpersontype"),
    path("docpersonnewform/<str:person_type>", views.doceditpersontype_get_blank_form, name="docpersonnewform"),
    path("docpersondelete/<int:docrolemapping_id>", views.docdeleteperson, name="docpersondelete" ),
    ###############################

    ### PAGES & REFERENCES ########
    path("doceditpagesreferences/<int:doc_id>", views.doceditpagesreferences, name="doceditpagesreferences"),
    path("docgetpagesreferences/<int:doc_id>", views.docgetpagesreferences, name="docgetpagesreferences"),

    ###############################
    

    path("docgeneratenumber/<int:doc_id>", views.docgeneratenumber, name="docgeneratenumber"),
    path("docupload/<int:doc_id>", views.docupload, name="docupload" ),
    ####################################

    path("submittolibrary/<int:doc_id>", views.submit_to_library, name="libsubmit"),
    path("viewdocument/<int:doc_id>", views.viewdocument, name="viewdocument"),
    path("viewpdfthumbnail/<int:doc_id>",views.viewpdfthumbnail, name="viewdocumentthumbnail"),
    
    ###################################

    path("docwithdraw/<int:doc_id>", views.docwithdraw, name="docwithdraw"),
    path("dateextension/<int:doc_id>", views.dateextension, name="dateextension"),

    ###################################

    path("settings", views.docsettings, name="docsettings"),

    ###############################
    path("error", views.error,name="error"),

    ################################
]

if settings.DEBUG:
    urlpatterns +=  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)