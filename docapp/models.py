from django.db import models
from .constants import PDF_FILE_LOCATION, SECURITY_CLASSIFICATION,CONTROL_STATUS
from .constants import DOCUMENT_STATUSES, DOCUMENT_ROLES, DOC_NUMBER_STATUSES
from .constants import ENTITY_PROJECT_CHOICES, PDF_THUMBNAIL_LOCATION
from django.utils import timezone
from dateutil.relativedelta import relativedelta
# Create your models here.

class DocumentStatus(models.Model):
    doc_status_id = models.AutoField(primary_key=True, verbose_name="Document Status Id")
    doc_status_name = models.CharField(max_length=50, verbose_name="Document Status Name", choices=DOCUMENT_STATUSES)
    doc_status_display_name = models.CharField(max_length=50, verbose_name="Document Status Display Name")
    doc_status_active = models.IntegerField(default=1, verbose_name="Document Status Active")

    def __str__(self):
        return self.doc_status_display_name
    
class Document(models.Model):
    
    def two_months_from_now():
        return timezone.now() + relativedelta(months=2)

    def default_document_status():
        return DocumentStatus.objects.get(doc_status_name=DOCUMENT_STATUSES[0][0])
    
    doc_id = models.AutoField(primary_key=True, verbose_name="Document Id")
    doc_title = models.CharField(max_length=200, verbose_name="Document Title")
    doc_file = models.FileField(upload_to=PDF_FILE_LOCATION, verbose_name="Document File", blank=True)
    approved = models.IntegerField(default=0, verbose_name="Approval Status")
    doc_num_page = models.IntegerField(default=0,verbose_name="Document Number of Pages")
    security_classification = models.CharField(max_length=50,choices=SECURITY_CLASSIFICATION, verbose_name="Security Classification", default=SECURITY_CLASSIFICATION[0][0])
    control_status = models.CharField(max_length=50, choices=CONTROL_STATUS, verbose_name="Control Status", default=CONTROL_STATUS[0][0])
    num_references = models.IntegerField(default=0, verbose_name="Number of References")
    abstract = models.TextField(default="", verbose_name="Document Abstract")
    edc = models.DateTimeField(verbose_name="Document EDC", default=two_months_from_now)
    created_date = models.DateTimeField(verbose_name="Document Creation Date", default=timezone.now)
    published_date = models.DateTimeField(verbose_name="Document Published Date", blank=True, null=True)
    parent_doc = models.ForeignKey("self",verbose_name="Parent Doc",blank=True, on_delete = models.SET_DEFAULT, default=None, null=True)   

    @property
    def document_number(self):
        doc_number = DocumentNumber.objects.filter(doc_id=self.doc_id).last()
        if doc_number:
            return doc_number.document_number
        else:
            return "XXXXXXXXX"
    
    doc_status = models.ForeignKey(DocumentStatus,null=True,verbose_name="Document Status", on_delete=models.SET_NULL, default=None)
    doc_type_general  = models.CharField(max_length=50, verbose_name="Document Type", choices=ENTITY_PROJECT_CHOICES)
    doc_first_page_image = models.ImageField(upload_to=PDF_THUMBNAIL_LOCATION, verbose_name="Document First Page Image", blank=True)
    
    doc_num_extensions_asked = models.IntegerField(default=0, verbose_name="Document Number of Extensions Asked")

    def __str__(self):
        return self.doc_title
class Employee(models.Model):
    emp_code = models.CharField(primary_key=True, max_length=10, verbose_name="Employee Code")
    emp_name = models.CharField(max_length=100, verbose_name="Employee Name")
    emp_email = models.EmailField(max_length=100, verbose_name="Employee Email")
    emp_active = models.IntegerField(default=1, verbose_name="Employee Active")
    emp_from_COWAA = models.IntegerField(default=1, verbose_name="Employee From COWAA")


    def __str__(self):
        return self.emp_code + "-" + self.emp_name


class Keywords(models.Model):
    keyword_id = models.AutoField(primary_key=True, verbose_name="Keyword Id")
    keyword_name = models.CharField(max_length=100, verbose_name="Keyword Name")
    keyword_active = models.IntegerField(default=1, verbose_name="Keyword Active")

    def __str__(self):
        return self.keyword_name

class DocumentRoles(models.Model):
    docrole_id = models.AutoField(primary_key=True, verbose_name="Document Role Id")
    doc_role_short_code = models.CharField(max_length=50, verbose_name="Document Role Short Code", choices=DOCUMENT_ROLES)
    doc_role_display_name = models.CharField(max_length=100, verbose_name="Document Role Display Name")
    doc_role_active = models.IntegerField(default=1, verbose_name="Document Role Active")

    def __str__(self):
        return self.doc_role_display_name

class DocumentNumberStatus(models.Model):
    doc_num_status_id = models.AutoField(primary_key=True, verbose_name="Document Number Status Id")
    doc_num_status_name = models.CharField(max_length=50, verbose_name="Document Number Status Name", choices=DOC_NUMBER_STATUSES)
    doc_num_status_display_name = models.CharField(max_length=50, verbose_name="Document Number Status Display Name")
    doc_num_status_active = models.IntegerField(default=1, verbose_name="Document Number Status Active")

    def __str__(self):
        return self.doc_num_status_display_name
    
class DocumentType(models.Model):
    doc_type_id = models.AutoField(primary_key=True, verbose_name="Document Type Id")
    doc_type_short_code = models.CharField(max_length=70, verbose_name="Document Type Short Code")
    doc_type_name = models.CharField(max_length=100, verbose_name="Document Type Name")
    doc_type_active = models.IntegerField(default=1, verbose_name="Document Type Active")

    def __str__(self):
        return self.doc_type_name

class Project(models.Model):
    project_id = models.AutoField(primary_key=True, verbose_name="Project ID")
    project_short_code = models.CharField(max_length=50, verbose_name="Project Short Code")
    project_name = models.CharField(max_length=100, verbose_name="Project Name")
    project_active = models.IntegerField(default=1, verbose_name="Project Active")
    project_from_cowaa = models.IntegerField(default=1, verbose_name="Project From COWAA")
    def __str__(self):
        return self.project_name
    
class AuditLog(models.Model):
    audit_id = models.AutoField(primary_key=True, verbose_name="Audit Id")
    audit_short_code = models.CharField(max_length=50, verbose_name="Audit Short Code")
    action_description = models.TextField(verbose_name="Action Description")
    action_date = models.DateTimeField(verbose_name="Action Date")
    action_by = models.CharField(max_length=50, verbose_name="Action By")

    def __str__(self):
        return "Audit Log ID" + str(self.audit_id)
    

class DocumentNumber(models.Model):
    docnumber_id = models.AutoField(primary_key=True, verbose_name="Document Number Id")
    doc = models.ForeignKey(Document, verbose_name="Document Id", on_delete=models.CASCADE)
    doc_project = models.ForeignKey(Project, null=True, blank=True, verbose_name="Project Code", on_delete=models.SET_NULL)
    doc_type = models.ForeignKey(DocumentType, null = True , blank=True, verbose_name="Document Type", on_delete=models.SET_NULL)
    doc_revision = models.IntegerField(default=0, verbose_name="Document Revision")
    doc_year = models.IntegerField(verbose_name="Document Year", default=timezone.now().year)
    document_sequence_number = models.IntegerField(default=0, verbose_name="Document Sequence Number")
    document_number_status = models.ForeignKey(DocumentNumberStatus,null=True,verbose_name="Document Number Status", on_delete=models.SET_NULL)
    date_created = models.DateTimeField(verbose_name="Date Created", default=timezone.now)

    @property
    def document_number(self):
        if(self.doc_project == None or self.doc_type == None or self.document_sequence_number == 0):
            return "XXXXXXXXX"

        if(self.doc_revision > 0):
            return (
                "LPSC/" + str(self.doc_project.project_short_code) + 
                "/" + str(self.doc_type.doc_type_short_code) + 
                "/" + str(self.document_sequence_number) + 
                "/" + str(self.doc_year) + 
                "/R" + str(self.doc_revision) +
                "/" + str(self.doc_year)
            )
        else:
            return (
                "LPSC/" + str(self.doc_project.project_short_code) + 
                "/" + str(self.doc_type.doc_type_short_code) +
                "/" + str(self.document_sequence_number) + 
                "/" + str(self.doc_year)
            )
    docnumber_text = models.CharField(max_length=100, verbose_name="Document Number", default="XXXXXXXXX")   

    def __str__(self):
        if(self.document_number):
            return "Document Number ID " + str(self.document_number)
        else:
            return "Document Number ID " + str(self.docnumber_id)

class DocNumberSequence(models.Model):
    docnumber_sequence_id = models.AutoField(primary_key=True, verbose_name="Document Number Sequence Id")
    doc_project = models.ForeignKey(Project, verbose_name="Project Code", on_delete=models.CASCADE)
    doc_type = models.ForeignKey(DocumentType, verbose_name="Document Type Id", on_delete=models.CASCADE)
    doc_year = models.IntegerField(default=0, verbose_name="Document Year")
    doc_sequence_number = models.IntegerField(default=0, verbose_name="Document Sequence Number")
    doc_sequence_active = models.IntegerField(default=1, verbose_name="Document Sequence Active")

    def __str__(self):
        return "Sequence " + str(self.doc_project.project_short_code) + " -- " + str(self.doc_type.doc_type_short_code)


class DocRoleMapping(models.Model):
    docrole_id = models.AutoField(primary_key=True, verbose_name="Document Role Mapping Id")
    doc = models.ForeignKey(Document, verbose_name="Document Id", on_delete=models.CASCADE)
    emp = models.ForeignKey(Employee,verbose_name="Employee Code", on_delete=models.CASCADE)
    empdisplay_name = models.TextField(verbose_name="Employee Display Name")
    role = models.ForeignKey(DocumentRoles, null = True ,verbose_name="Role Id", on_delete=models.SET_NULL)
    print_order = models.IntegerField(default=0, verbose_name="Print Order")
    role_status = models.IntegerField(default=1, verbose_name="Role Status")

    def __str__(self):
        return "Document" + str(self.doc.doc_id) + " -- Employee " + str(self.emp.emp_code)


class DocKeywordMapping(models.Model):
    doc_keyword_mapping_id = models.AutoField(primary_key=True, verbose_name="Document Keyword Mapping Id")
    doc = models.ForeignKey(Document, verbose_name="Document Id", on_delete=models.CASCADE)
    keyword = models.ForeignKey(Keywords, verbose_name="Keyword Id", on_delete=models.CASCADE)
    keyword_weight = models.IntegerField(default=0, verbose_name="Keyword Weight")
    keyword_active = models.IntegerField(default=1, verbose_name="Keyword Active")

    def __str__(self):
        return "Document" + str(self.doc.doc_id) + " -- Keyword" + str(self.keyword.keyword_id)
    
class DocumentActivity(models.Model):
    document_activity_id = models.AutoField(primary_key=True, verbose_name="Document Activity Id")
    doc = models.ForeignKey(Document,null = True, verbose_name="Document Id", on_delete=models.SET_NULL)
    doc_activity_description = models.TextField(verbose_name="Document Activity Description")
    doc_activity_date = models.DateTimeField(verbose_name="Document Activity Date")
    doc_status = models.ForeignKey(DocumentStatus, null = True, verbose_name="Document Status Id", on_delete=models.SET_NULL)

    def __str__(self):
        return "Document" + str(self.doc.doc_id) + " -- Activity" + str(self.document_activity_id)
