
var entity_project = document.getElementById('id_doc_project');
const enity_project_choices = new Choices(entity_project,{
    allowHTML : true,
});

var report_type = document.getElementById('id_doc_type');
const report_type_choices = new Choices(report_type,{
    allowHTML : true,
});

const authortable = "author_tablebody";
const checkbytable = "checkedby_tablebody";
const reviewertable = "reviewer_tablebody";
const approvertable = "approver_tablebody";

htmx.on("htmx:afterSwap", (event) => {
    if(event.detail.target.id === authortable || 
        event.detail.target.id === checkbytable ||
        event.detail.target.id === reviewertable ||
        event.detail.target.id === approvertable
    )
    {
        var person_type = event.detail.target.id.split("_")[0]
        var totalforms_id = "id_" + person_type + "-TOTAL_FORMS";
        var  totalforms= document.getElementById(totalforms_id);
        totalforms.value = parseInt(totalforms.value) + 1;
    }
});

function removeForm(ele,form_type)
{
    var current_element_id = ele.getAttribute('id')
    splitted_ids  = current_element_id.split("_");
    console.log('Reached here');
    if(splitted_ids.length == 4 )
    {
        if(!isNaN(splitted_ids[2]))
        {
            var form_delete_check_button = "id_" + form_type +"-" + splitted_ids[2]  + "-DELETE"
            var delete_checkbox = document.getElementById(form_delete_check_button);
            delete_checkbox.setAttribute('checked', 'checked');
            ele.closest('tr').style.display="none";

        }
    }
}