const notyf_message = new Notyf({
    position: {
        x: 'right',
        y: 'top',
    },
    types: [
        {
            type: 'warning',
            background: 'orange',
            dismissible: true
        },
        {
            type: 'success',
            background: 'green',
            dismissible: true
        },
        {
            type: 'error',
            background: 'red',
            dismissible: true
        }
    ]
});


// Some constants to be checked out
const titleabstractmodal_name = "modal_title_abstract";
const titleabstractdialog_name = "modal_title_abstract_form";
const keywordsmodal_name = "modal_keywords";
const keywordsdialog_name = "modal_keywords_form";
const docmodalprojectreporttypesmodal_name  = "modal_project_reporttypes";
const docmodalprojectreporttypesdialog_name = "modal_project_reporttypes_form"; 
const docpersontypemodal_name = "modal_person_type";
const docpersontypedialog_name = "modal_person_type_form";
const docnumpagesreferencesmodal_name = "modal_pages_references";
const docnumpagesreferencesdialog_name = "modal_pages_references_form";

const authortable = "author_tablebody";
const checkbytable = "checkedby_tablebody";
const reviewertable = "reviewer_tablebody";
const approvertable = "approver_tablebody";


const titleabstractmodal = new bootstrap.Modal(document.getElementById(titleabstractmodal_name));
const keywordsmodal = new bootstrap.Modal(document.getElementById(keywordsmodal_name));
const docmodalprojectreporttypesmodal = new bootstrap.Modal(document.getElementById(docmodalprojectreporttypesmodal_name));
const docpersontypemodal = new bootstrap.Modal(document.getElementById(docpersontypemodal_name));
const docnumpagesreferencesmodal = new bootstrap.Modal(document.getElementById(docnumpagesreferencesmodal_name));

htmx.on("htmx:afterSwap", (event) => {
    if(event.detail.target.id == titleabstractdialog_name)
    {
        titleabstractmodal.show();
    }
    else if(event.detail.target.id == keywordsdialog_name)
    {
        var keywordsInputEl = d.querySelector('#id_keywords');
        if (keywordsInputEl) {
            const choices = new Choices(keywordsInputEl,{
                allowHTML : true,
            });
        }
        keywordsmodal.show();
    }
    else if(event.detail.target.id == docmodalprojectreporttypesdialog_name)
    {
        var selectProject = d.querySelector('#id_doc_project');
        if(selectProject) {
            const projectchoices = new Choices(selectProject,{
                allowHTML : true,
            });
        }
        var selectReportType = d.querySelector('#id_doc_type');
        if(selectProject) {
            const reporttypeSelect = new Choices(selectReportType,{
                allowHTML : true,
            });
        }

        docmodalprojectreporttypesmodal.show();
    }
    else if(event.detail.target.id == docpersontypedialog_name)
    {
        docpersontypemodal.show();
    }
    else if(event.detail.target.id == docnumpagesreferencesdialog_name)
    {
        docnumpagesreferencesmodal.show();
    }
});

htmx.on("htmx:beforeSwap", (event) => {
    if(event.detail.target.id == titleabstractdialog_name && !event.detail.xhr.response)
    {
        titleabstractmodal.hide();
        event.detail.shouldSwap = false;
    }
    else if(event.detail.target.id == keywordsdialog_name && !event.detail.xhr.response)
    {
        keywordsmodal.hide();
        event.detail.shouldSwap = false;
    }
    else if(event.detail.target.id == docmodalprojectreporttypesdialog_name && !event.detail.xhr.response)
    {
        docmodalprojectreporttypesmodal.hide();
        event.detail.shouldSwap = false;
    }
    else if(event.detail.target.id == docnumpagesreferencesdialog_name && !event.detail.xhr.response)
    {
        docnumpagesreferencesmodal.hide();
        event.detail.shouldSwap = false;
    }
    else if(event.detail.target.id == docpersontypedialog_name && !event.detail.xhr.response)
    {
        docpersontypemodal.hide();
        event.detail.shouldSwap = false;
    }
});

htmx.on("hidden.bs.modal", (event) => {
    document.getElementById(event.target.id+"_form").innerHTML = "";
});

htmx.on("htmx:afterSwap", (event) => {
    if(event.detail.target.id === authortable || 
        event.detail.target.id === checkbytable ||
        event.detail.target.id === reviewertable ||
        event.detail.target.id === approvertable
    )
    {
        var person_type = event.detail.target.id.split("_")[0]
        var totalforms_id = "id_" + person_type + "-TOTAL_FORMS";
        var  totalforms= document.getElementById(totalforms_id);
        totalforms.value = parseInt(totalforms.value) + 1;
        console.log(totalforms.value);
    }
});

function removeForm(ele,person_type)
{
    var current_element_id = ele.getAttribute('id')
    splitted_ids  = current_element_id.split("_");
    if(splitted_ids.length == 4 )
    {
        if(!isNaN(splitted_ids[2]))
        {
            var form_delete_check_button = "id_" + person_type + "-" + splitted_ids[2]  + "-DELETE"
            var delete_checkbox = document.getElementById(form_delete_check_button);
            delete_checkbox.setAttribute('checked', 'checked');
            ele.closest('tr').style.display="none";

        }
    }
}

var docpreviewbutton = document.getElementById("docpreviewbutton");
if(docpreviewbutton)
{
    docpreviewbutton.addEventListener("click", function(event){
        var url = docpreviewbutton.getAttribute("data-url");
        window.location.href= url;
    });
}