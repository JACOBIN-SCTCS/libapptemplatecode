from django.shortcuts import redirect, render

from docapp.constants import DOC_NUMBER_DRAFT, DOC_NUMBER_STATUSES, DOC_NUMBER_USED, DOCUMENT_STATUSES, MEMBER_TYPES, DOCUMENT_ROLES, PDF_FILE_LOCATION
from docapp.constants import AUTHOR_CONSTANT, REVIEWER_CONSTANT, APPROVER_CONSTANT, CHECKED_BY_CONSTANT
from docapp.constants import AUTHOR_TEXT,REVIEWER_TEXT,APPROVER_TEXT,CHECKED_BY_TEXT, PERSON_CONSTANT_TO_TEXT_MAPPING
from docapp.constants import DOCUMENT_CONSTANT_TO_DB_MAPPING
from docapp.constants import DOC_DRAFT,DOC_PENDING_UPLOAD,DOC_UNDER_REVIEW,DOC_REUPLOAD,DOC_NUMBER_WITHDRAWN,DOC_COMPLETED
from docapp.constants import DOC_AUTHOR_CONSTANT, DOC_CHECKED_CONSTANT, DOC_APPROVER_CONSTANT, DOC_REVIEWER_CONSTANT
from docapp.constants import DOCUMENT_LIST_PAGE_SIZE, PDF_THUMBNAIL_LOCATION, NUM_EXTENSIONS_DAYS, LIMIT_EXTENSIONS

from .models import DocNumberSequence, Document, DocKeywordMapping, DocumentRoles, DocumentStatus, Employee, Keywords, DocumentNumber
from .models import DocumentNumberStatus, DocRoleMapping
from .forms import DocumentCreationForm, DocumentTitleAbstractForm, DocumentUploadForm
from .forms import DocumentKeywordsForm, DocumentEntityReportTypeForm, DocumentPagesReferencesForm
from .forms import DocumentAdvancedSearch, DocumentNumberSearch, DocumentPersonForm
from .forms import DocumentPersonBaseFormSet
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.shortcuts import get_object_or_404

from django.forms import formset_factory
from django.contrib import messages
from django.utils import timezone
from django.core.paginator import Paginator
from django.db import transaction
from django.urls import reverse
from django.conf import settings
import hashlib
import os
import fitz
# Create your views here.

#### Some View Related Constants ############

REDIRECT_URL = "docdetails/{0}"

# Dont change the left hand side of the dictionary
HX_TRIGGERS = {
    "basicdetails": "basicdetailschanged",
    "entityreporttype": "entityreporttypechanged",
    "keywords" : "keywordschanged",
    "pagesreferences": "pagesreferenceschanged",
    AUTHOR_CONSTANT : "authorschanged",
    REVIEWER_CONSTANT : "reviewerschanged",
    APPROVER_CONSTANT : "approverschanged",
    CHECKED_BY_CONSTANT : "checkedbychanged"
}

ADVANCED_SEARCH_FORM_SUBMIT_ID = 'advancedsearch_submit'
SIMPLE_SEARCH_FORM_SUBMIT_ID = 'simplesearch_submit'
ADVANCED_SEARCH_FORM_HIDDEN_FIELD = 'advancedsearch_form_hidden'
SIMPLE_SEARCH_FORM_HIDDEN_FIELD = 'simplesearch_form_hidden'

#ADVANCED_SEARCH_TOGGLE_NAME = 'search_toggle_button_hidden'
ADVANCED_SEARCH_TOGGLE_NAME = 'search_toggle_button'

##########################################


def index(request):
    return render(request, "docapp/index.html", {})


def save_people_data(formset, person_type, doc_id):
    try:
        person_db_value = DOCUMENT_CONSTANT_TO_DB_MAPPING[person_type]
        document = Document.objects.get(doc_id = doc_id)
        document_role = DocumentRoles.objects.filter(doc_role_short_code=person_db_value).first()

        formset_cleaned_data = []
        for form in formset:
            if 'DELETE' in form.cleaned_data:
                if (not form.cleaned_data['DELETE']):
                    formset_cleaned_data.append(form.cleaned_data)
            else:
                formset_cleaned_data.append(form.cleaned_data)          
                
        for form_data in formset_cleaned_data:
            if 'person' in form_data:
                person_doc_role = DocRoleMapping(
                    doc=document,
                    emp=form_data['person'],
                    empdisplay_name = "",
                    role=document_role,
                    print_order=form_data['print_order'],
                    role_status = 1
                ) 
                person_doc_role.save()
        return True
    except:
        return False

def generate_document_sequence_number(project, doc_type, current_year):
    
    STARTING_SEQUENCE_NUMBER = 1000
    try:
        docnumber_sequence = DocNumberSequence.objects.get(
                                doc_project = project,
                                doc_type = doc_type,
                                doc_year = current_year,
                                doc_sequence_active = 1
                            )
    except:
        docnumber_sequence = None
    
    if(docnumber_sequence is None):
        docnumber_sequence = DocNumberSequence(
                        doc_project = project,
                        doc_type = doc_type,
                        doc_year = current_year,
                        doc_sequence_active = 1,
                        doc_sequence_number = STARTING_SEQUENCE_NUMBER
            )
        docnumber_sequence.save()
    
    sequence_number =  docnumber_sequence.doc_sequence_number
    docnumber_sequence.doc_sequence_number = docnumber_sequence.doc_sequence_number + 1
    docnumber_sequence.save()

    return sequence_number

def get_non_field_form_error_messages(request,form, is_formset=True ):
    error_messages = []
    if is_formset:
        errors_list = form.non_form_errors()
    else:
        errors_list = form.non_field_errors()
    for error in errors_list:
        messages.add_message(request,messages.ERROR,error)
    
def handle_file_upload(doc_id,file):
    document = Document.objects.get(doc_id = doc_id)
    
    m = hashlib.sha256()
    document_number = document.document_number
    document_number = document_number.replace("/","_")

    # Hash for document content
    content_to_encrypt = document_number + "_" + document.doc_title + "#" + document.abstract
    m.update(str.encode(content_to_encrypt))
    hex_digest = m.hexdigest()

    file_name = document_number + "_" + hex_digest + ".pdf"
    complete_path = os.path.join(PDF_FILE_LOCATION,file_name)
    if not os.path.exists(PDF_FILE_LOCATION):
        os.makedirs(PDF_FILE_LOCATION)
        
    with open(complete_path,"wb+") as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    
    # Generate Thumbnail
    pdf_document = fitz.open(complete_path)
    first_page = pdf_document[0]
    rect = fitz.Rect(0, 0, first_page.rect.width,first_page.rect.height)
    #first_page.add_rect_annot(rect)
    shape=first_page.new_shape()
    shape.draw_rect(rect)
    shape.finish(color=(0, 0, 0),width=2.5)
    shape.commit()
    image = first_page.get_pixmap()
    if not os.path.exists(PDF_THUMBNAIL_LOCATION):
        os.makedirs(PDF_THUMBNAIL_LOCATION)    

    image_path = os.path.join(PDF_THUMBNAIL_LOCATION,document_number + ".png")
    image.save(image_path)
    
    return (complete_path,image_path)

def newdoc(request):
    if request.method == "POST":
        form  = DocumentCreationForm(request.POST)
        report_type_form = DocumentEntityReportTypeForm(request.POST)
        
        DocumentPersonFormSet = formset_factory(DocumentPersonForm,formset=DocumentPersonBaseFormSet,extra=1, can_delete=True)
        author_formset = DocumentPersonFormSet(request.POST, prefix=AUTHOR_CONSTANT)
        reviewer_formset = DocumentPersonFormSet(request.POST,prefix = REVIEWER_CONSTANT)
        approver_formset = DocumentPersonFormSet(request.POST, prefix=APPROVER_CONSTANT)
        checkedby_formset = DocumentPersonFormSet(request.POST,prefix=CHECKED_BY_CONSTANT)

        # Reduce the below paragraph of code with a few lines
        form_valid = form.is_valid()
        report_type_form_valid = report_type_form.is_valid()
        author_formset_valid = author_formset.is_valid()
        reviewer_formset_valid = reviewer_formset.is_valid()
        approver_formset_valid = approver_formset.is_valid()
        # IGNORED AS OF NOW
        checkedby_formset_valid = checkedby_formset.is_valid()
        
        # Check if all entries are true
        if(form_valid==report_type_form_valid==author_formset_valid==reviewer_formset_valid==approver_formset_valid==True):

            doc_status_object = None
            doc_number_status_object = None
          
            if 'generate' in request.POST:
                doc_status_object = DocumentStatus.objects.get(doc_status_name=DOCUMENT_STATUSES[1][0])
                doc_number_status_object = DocumentNumberStatus.objects.get(doc_num_status_name=DOC_NUMBER_STATUSES[1][0])
            else:
                doc_status_object = DocumentStatus.objects.get(doc_status_name=DOCUMENT_STATUSES[0][0])
                doc_number_status_object = DocumentNumberStatus.objects.get(doc_num_status_name=DOC_NUMBER_STATUSES[0][0] )

            try:
                with transaction.atomic():
                        new_document = Document.objects.create(
                            doc_title=form.cleaned_data['doc_title'],
                            control_status=form.cleaned_data['control_status'],
                            security_classification=form.cleaned_data['security_classification'],
                            doc_type_general=form.cleaned_data['doc_type_general'],
                            doc_status=doc_status_object
                        )

                        new_document.parent_doc = new_document
                        new_document.save()

                        save_people_data(author_formset,AUTHOR_CONSTANT,new_document.doc_id)
                        save_people_data(reviewer_formset,REVIEWER_CONSTANT,new_document.doc_id)
                        save_people_data(approver_formset,APPROVER_CONSTANT,new_document.doc_id)
                        
                        if('generate' in request.POST):
                            current_year = timezone.now().year
                            sequence_number = generate_document_sequence_number(report_type_form.cleaned_data['doc_project'],report_type_form.cleaned_data['doc_type'],current_year)
                            doc_number = DocumentNumber.objects.create(
                                doc=new_document,
                                doc_project = report_type_form.cleaned_data['doc_project'],
                                doc_type = report_type_form.cleaned_data['doc_type'],
                                doc_year = current_year,
                                document_sequence_number = sequence_number,
                                document_number_status=doc_number_status_object
                            )
                            doc_number.docnumber_text = doc_number.document_number
                            doc_number.save()
                        else:
                            doc_number = DocumentNumber.objects.create(
                                doc=new_document,
                                doc_project = report_type_form.cleaned_data['doc_project'],
                                doc_type = report_type_form.cleaned_data['doc_type'],
                                document_number_status=doc_number_status_object
                            )
                            doc_number.save()
                return redirect(reverse("docapp:docdetails",kwargs={"doc_id": int(new_document.doc_id)}))       
            except:
                print("Database error")
        else:
            # Display error messages coming from report type form
            get_non_field_form_error_messages(request,report_type_form,False)
            get_non_field_form_error_messages(request,author_formset)
            get_non_field_form_error_messages(request,approver_formset)
            get_non_field_form_error_messages(request,reviewer_formset)
    else:
        form = DocumentCreationForm()
        report_type_form = DocumentEntityReportTypeForm()
        
        DocumentPersonFormSet = formset_factory(DocumentPersonForm,formset=DocumentPersonBaseFormSet,extra=1, can_delete=True)
        author_formset = DocumentPersonFormSet(prefix=AUTHOR_CONSTANT)
        reviewer_formset = DocumentPersonFormSet(prefix=REVIEWER_CONSTANT)
        approver_formset = DocumentPersonFormSet(prefix=APPROVER_CONSTANT)
        checkedby_formset = DocumentPersonFormSet(prefix=CHECKED_BY_CONSTANT)

    return render(request, "docapp/new_doc.html", {
        'form': form, 'report_type_form': report_type_form,
        'author_formset': author_formset,
        'reviewer_formset': reviewer_formset,
        'approver_formset': approver_formset,
        'checkedby_formset': checkedby_formset,

        "AUTHOR_CONSTANT" : AUTHOR_CONSTANT,
        "REVIEWER_CONSTANT" : REVIEWER_CONSTANT,
        "APPROVER_CONSTANT" : APPROVER_CONSTANT,
        "CHECKED_BY_CONSTANT" : CHECKED_BY_CONSTANT,

    })


def doclist(request, person_type):
    # GET ALL DOCUMENTS ORDER BY doc_id in descending order
    if person_type not in MEMBER_TYPES:
        return HttpResponseNotFound("Page Not Found")

    document_advanced_search_form = DocumentAdvancedSearch()
    document_number_search_form = DocumentNumberSearch()
    documents = Document.objects.all().order_by('-doc_id')
    
    paginator = Paginator(documents,DOCUMENT_LIST_PAGE_SIZE)
    page = paginator.get_page(1)
    next_page_exists = page.has_next()
    
    return render(request, "docapp/doc_list.html", {
        "doc_list": page.object_list,
        "doc_number_search": document_number_search_form,
        "person_type": person_type,
        "adv_search": document_advanced_search_form,
        "include_form" : "simplesearch_form",
        "next_page_exists" : next_page_exists,
        "next_page_number" : 2
    })


def doclist_filtered(request, person_type):

    if (person_type not in MEMBER_TYPES):
        return render(request, "docapp/doc_list_page_templates/list_template.html", {"doc_list": None})
    
    print(request.GET)
    
    PAGE_TO_FETCH = 1 
    include_form = "simplesearch_form"

    if ADVANCED_SEARCH_FORM_HIDDEN_FIELD in request.GET:
        form_data = DocumentAdvancedSearch(request.GET)
        advanced_filter_arguments = {}
        if (form_data.is_valid()):
            authors_name = form_data.cleaned_data['authors_name']
            reviewers_name = form_data.cleaned_data['reviewers_name']
            approvers_name = form_data.cleaned_data['approvers_name']
            document_status = form_data.cleaned_data['document_status']
            document_title = form_data.cleaned_data['document_title']

            all_fields_empty = True
            doc_ids = None
            if (authors_name != None and authors_name != ""):
                all_fields_empty = False
                auth_set = set(DocRoleMapping.objects.filter(
                    role__doc_role_short_code=DOCUMENT_ROLES[0][0], emp__emp_name__icontains=authors_name).values_list('doc_id', flat=True))
                if doc_ids == None:
                    doc_ids = auth_set
                else:
                    doc_ids = doc_ids.intersection(auth_set)
            if (reviewers_name != None and reviewers_name != ""):
                all_fields_empty = False
                rev_set = set(DocRoleMapping.objects.filter(
                    role__doc_role_short_code=DOCUMENT_ROLES[2][0], emp__emp_name__icontains=reviewers_name).values_list('doc_id', flat=True))
                if doc_ids == None:
                    doc_ids = rev_set
                else:
                    doc_ids = doc_ids.intersection(rev_set)
            if (approvers_name != None and approvers_name != ""):
                all_fields_empty = False
                app_set = set(DocRoleMapping.objects.filter(
                    role__doc_role_short_code=DOCUMENT_ROLES[3][0], emp__emp_name__icontains=approvers_name).values_list('doc_id', flat=True))
                if doc_ids == None:
                    doc_ids = app_set
                else:
                    doc_ids = doc_ids.intersection(app_set)

            if(document_status != "ALL"):
                all_fields_empty = False
                status_ids = set(Document.objects.filter(
                    doc_status__doc_status_name = document_status).values_list('doc_id', flat=True))
                if doc_ids == None:
                    doc_ids = status_ids
                else:
                    doc_ids = doc_ids.intersection(status_ids)
            if (document_title != None and document_title != ""):
                all_fields_empty = False
                title_ids = set(Document.objects.filter(
                    doc_title__icontains=document_title).values_list('doc_id', flat=True))
                if doc_ids == None:
                    doc_ids = title_ids
                else:
                    doc_ids = doc_ids.intersection(title_ids)
            if(all_fields_empty):
                documents = Document.objects.all().order_by('-doc_id')
            else:
                documents = Document.objects.filter(doc_id__in=doc_ids).order_by('-doc_id')
        include_form = "advancedsearch_form"
    elif SIMPLE_SEARCH_FORM_HIDDEN_FIELD in request.GET:
        form_data = DocumentNumberSearch(request.GET)
        if (form_data.is_valid()):
            doc_number  = form_data.cleaned_data['document_number']
            if doc_number != None and doc_number != "":
               doc_ids = DocumentNumber.objects.filter(docnumber_text__icontains = doc_number,document_number_status__doc_num_status_name__in =[DOC_NUMBER_DRAFT,DOC_NUMBER_USED]).values_list('doc_id', flat=True).distinct()
               documents = None
               if doc_ids != None:
                   documents = Document.objects.filter(doc_id__in=doc_ids).order_by('-doc_id')   
            else:
                documents = Document.objects.all().order_by('-doc_id')
        include_form = "simplesearch_form"    
    elif ADVANCED_SEARCH_TOGGLE_NAME in request.GET:
        print("Reached here for toggle")
        documents = Document.objects.all().order_by('-doc_id')
        include_form = "advancedsearch_form"
    else:
        documents = Document.objects.all().order_by('-doc_id')
        include_form = "simplesearch_form"
    
    paginator = Paginator(documents,per_page=DOCUMENT_LIST_PAGE_SIZE)
    if('page' in request.GET):
        PAGE_TO_FETCH = int(request.GET['page'])
    
    page = paginator.get_page(PAGE_TO_FETCH)
    next_page_exists = page.has_next()
    next_page_number  = PAGE_TO_FETCH + 1


    return render(request, "docapp/doc_list_page_templates/list_template.html", {
        "doc_list": page.object_list,
        "person_type" : person_type,
        "include_form" : include_form,
        "next_page_exists" : next_page_exists,
        "next_page_number" : next_page_number
    })


def docdetails(request, doc_id):
    # document = Document.objects.get(doc_id=doc_id)
    document = get_object_or_404(Document, doc_id=doc_id)
    keywords = DocKeywordMapping.objects.filter(doc__doc_id=doc_id)

    try:
        docnumber = DocumentNumber.objects.filter(
                doc__doc_id=doc_id).last()
    except DocumentNumber.DoesNotExist:
        docnumber = None

    doc_authors = DocRoleMapping.objects.filter(doc__doc_id=doc_id, role__doc_role_short_code=DOCUMENT_CONSTANT_TO_DB_MAPPING[AUTHOR_CONSTANT],role_status=1).order_by('print_order')
    doc_checkedby = DocRoleMapping.objects.filter(doc__doc_id=doc_id, role__doc_role_short_code=DOCUMENT_CONSTANT_TO_DB_MAPPING[CHECKED_BY_CONSTANT],role_status=1).order_by('print_order')
    doc_reviewers = DocRoleMapping.objects.filter(doc__doc_id=doc_id, role__doc_role_short_code=DOCUMENT_CONSTANT_TO_DB_MAPPING[REVIEWER_CONSTANT],role_status=1).order_by('print_order')
    doc_approvers = DocRoleMapping.objects.filter(doc__doc_id=doc_id, role__doc_role_short_code=DOCUMENT_CONSTANT_TO_DB_MAPPING[APPROVER_CONSTANT],role_status=1).order_by('print_order')
    
    document_upload_form = None
    if(document.doc_status.doc_status_name == DOC_PENDING_UPLOAD or document.doc_status.doc_status_name == DOC_REUPLOAD):
        document_upload_form = DocumentUploadForm()
         
    return render(request, "docapp/doc_details.html", {
        "doc_id": doc_id,
        "doc": document,
        "docnumber": docnumber,
        "keywords": keywords,
        "triggers": HX_TRIGGERS,

        ######### Peoples associated with the document ####
        "authors" : doc_authors,
        "checkedby" : doc_checkedby,
        "reviewers" : doc_reviewers,
        "approvers" : doc_approvers,

        ######### Document Upload form ####################
        "document_upload_form" : document_upload_form,

        ####### CONSTANTS PASSED TO THE TEMPLATE ########
        "NUMBER_PENDING_ASSIGNMENT": DOC_NUMBER_STATUSES[0][0],
        "NUMBER_WITHDRAWN": DOC_NUMBER_STATUSES[2][0],
        "AUTHOR_CONSTANT": AUTHOR_CONSTANT,
        "REVIEWER_CONSTANT": REVIEWER_CONSTANT,
        "APPROVER_CONSTANT": APPROVER_CONSTANT,
        "CHECKED_BY_CONSTANT": CHECKED_BY_CONSTANT,
        "AUTHOR_TEXT" : AUTHOR_TEXT,
        "REVIEWER_TEXT" : REVIEWER_TEXT,
        "APPROVER_TEXT" : APPROVER_TEXT,
        "CHECKED_BY_TEXT" : CHECKED_BY_TEXT, 
        "DOC_DRAFT" : DOC_DRAFT,
        "DOC_PENDING_UPLOAD" : DOC_PENDING_UPLOAD,
        "DOC_UNDER_REVIEW" : DOC_UNDER_REVIEW,
        "DOC_REUPLOAD" : DOC_REUPLOAD,
        "DOC_NUMBER_WITHDRAWN" : DOC_NUMBER_WITHDRAWN,
        "DOC_COMPLETED" : DOC_COMPLETED
    })


##### FOR Document Title, Abstract , Control Status and Security Classification ##########

def docgetbasic(request, doc_id):
    document = Document.objects.get(doc_id=doc_id)
    return render(request, "docapp/doc_details_page_templates/doc_title_abstract.html", {"doc_id": doc_id, "doc": document})


def doceditbasic(request, doc_id):
    form = None

    if request.method == "POST":
        form = DocumentTitleAbstractForm(request.POST)
        if form.is_valid():
            document = Document.objects.get(doc_id=doc_id)
            document.doc_title = form.cleaned_data['doc_title']
            document.abstract = form.cleaned_data['abstract']
            document.control_status = form.cleaned_data['control_status']
            document.security_classification = form.cleaned_data['security_classification']
            document.save()
            return HttpResponse(status=204, headers={'HX-Trigger': HX_TRIGGERS['basicdetails']})
    else:
        document = Document.objects.get(doc_id=doc_id)
        form = DocumentTitleAbstractForm(instance=document)
    return render(request, "docapp/doc_details_page_templates/doc_title_abstract_modal_form.html", {"form": form, "doc_id": doc_id})
############################################################################################

##### FOR Document Keywords ###############################################################


def doceditkeywords(request, doc_id):
    form = DocumentKeywordsForm()
    if request.method == "POST":
        form = DocumentKeywordsForm(request.POST)
        if form.is_valid():
            keywords = form.cleaned_data['keywords']
            try:
                with transaction.atomic():
                    existing_keywords = DocKeywordMapping.objects.filter(doc__doc_id=doc_id)
                    for e in existing_keywords:
                        e.delete()
                
                    document = Document.objects.get(doc_id = doc_id)
                    for k in keywords:
                        mapping = DocKeywordMapping(doc = document, keyword = k)
                        mapping.save()
                    return HttpResponse(status=204, headers={'HX-Trigger': HX_TRIGGERS['keywords']})
            except Exception as error:
                print(error)
    else:
        doc_keyword_mapping = DocKeywordMapping.objects.filter(doc__doc_id = doc_id,keyword_active=1)
        keywords = [row.keyword  for row in doc_keyword_mapping]
        form = DocumentKeywordsForm(initial={'keywords' : keywords})
    return render(request, "docapp/doc_details_page_templates/doc_keywords_modal_form.html", {"keywords_form": form, "doc_id": doc_id})


def docgetkeywords(request, doc_id):
    keywords = DocKeywordMapping.objects.filter(doc_id=doc_id)
    return render(request, "docapp/doc_details_page_templates/doc_keywords.html", {"doc_id": doc_id, "keywords": keywords})


###########################################################################################


######### FOR PROJECT & REPORT TYPE ##########################################################
def doceditentityreporttype(request, doc_id):
    entity_report_type_form = None
    if request.method == "POST":
        entity_report_type_form = DocumentEntityReportTypeForm(request.POST)
        if entity_report_type_form.is_valid():
            docnumber = DocumentNumber.objects.filter(
                doc__doc_id=doc_id).last()
            docnumber.doc_project = entity_report_type_form.cleaned_data['doc_project']
            docnumber.doc_type = entity_report_type_form.cleaned_data['doc_type']
            docnumber.save()
            return HttpResponse(status=204, headers={'HX-Trigger': HX_TRIGGERS['entityreporttype']})
    else:
        docnumber = DocumentNumber.objects.filter(
                doc__doc_id=doc_id).last()
        entity_report_type_form = DocumentEntityReportTypeForm(
            instance=docnumber)

    return render(request, "docapp/doc_details_page_templates/doc_project_reporttype_modal_form.html", {"doc_id": doc_id, "entity_report_type_form": entity_report_type_form})


def docgetentityreporttype(request, doc_id):
    doc = Document.objects.get(doc_id=doc_id)
    docnumber = DocumentNumber.objects.filter(
                doc__doc_id=doc_id).last()
    return render(request, "docapp/doc_details_page_templates/doc_project_reporttype.html", {"doc_id": doc_id, "docnumber": docnumber, "doc": doc})
###############################################################################################

### For Person Type ###########################################################################
def docgetpersontype(request,doc_id,person_type):
    person_db_value = DOCUMENT_CONSTANT_TO_DB_MAPPING[person_type]
    people = DocRoleMapping.objects.filter(doc__doc_id=doc_id, role__doc_role_short_code=person_db_value,role_status=1).order_by('print_order')
    return render(request,"docapp/doc_details_page_templates/doc_peoples.html",{"doc_id" : doc_id, "people":people,"person_type" :person_type , "person_type_text" : PERSON_CONSTANT_TO_TEXT_MAPPING[person_type] })

def doceditpersontype(request, doc_id, person_type):
    person_db_value = DOCUMENT_CONSTANT_TO_DB_MAPPING[person_type]

    persons = DocRoleMapping.objects.filter(doc__doc_id=doc_id, role__doc_role_short_code=person_db_value,role_status=1).order_by('print_order')
    if len(persons) > 0:
        DocumentPersonFormSet = formset_factory(DocumentPersonForm,formset=DocumentPersonBaseFormSet,extra=0, can_delete=True)
    else:
        DocumentPersonFormSet = formset_factory(DocumentPersonForm,formset=DocumentPersonBaseFormSet,extra=1, can_delete=True)

    
    document = Document.objects.get(doc_id = doc_id)
    document_role = DocumentRoles.objects.filter(doc_role_short_code=person_db_value).first()

    if(request.method=="POST"):
        print(request.POST)
        formset = DocumentPersonFormSet(request.POST, prefix=person_type)
        formset_cleaned_data = []
        if formset.is_valid():
            for form in formset:
               print(form.cleaned_data)
               if 'DELETE' in form.cleaned_data:
                   if (not form.cleaned_data['DELETE']):
                       formset_cleaned_data.append(form.cleaned_data)
               else:
                   formset_cleaned_data.append(form.cleaned_data)          
            # DELETE ALL PREVIOUS ENTRIES IF THEY EXIST AND ADD THE NEW ONES
            for p in persons:
                p.delete()
        
            for form_data in formset_cleaned_data:
                if 'person' in form_data:
                    person_doc_role = DocRoleMapping(
                        doc=document,
                        emp=form_data['person'],
                        empdisplay_name = "",
                        role=document_role,
                        print_order=form_data['print_order'],
                        role_status = 1
                    ) 
                    person_doc_role.save()
        
            return HttpResponse(status=204, headers={'HX-Trigger': HX_TRIGGERS[person_type]}) 
        #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


    else:
        initial_data = []
        for p in persons:
            initial_data.append({
                'person' : p.emp,
                'print_order' : p.print_order,
                'doc_role_mapping_id' : p.docrole_id
            })
        
        formset = DocumentPersonFormSet(initial=initial_data, prefix=person_type)
    return render(request,"docapp/doc_details_page_templates/doc_peoples_modal_form.html",{"doc_id":doc_id, "person_type":person_type, "person_type_text" : PERSON_CONSTANT_TO_TEXT_MAPPING[person_type] ,"document_person_forms" : formset})

def doceditpersontype_get_blank_form(request, person_type):
    #form = DocumentPersonForm()
    total_forms_request_parameter = person_type + '-TOTAL_FORMS'
    new_form_number = request.GET[total_forms_request_parameter]
    if(new_form_number is None or len(new_form_number)==0):
        return HttpResponse("")

    DocumentPersonFormSet = formset_factory(DocumentPersonForm, formset=DocumentPersonBaseFormSet,extra=0, can_delete=True)
    empty_formset = DocumentPersonFormSet(prefix=person_type).empty_form
    empty_formset.prefix = empty_formset.prefix.replace("__prefix__",new_form_number)
    return render(request,"docapp/doc_details_page_templates/doc_peoples_form.html",{"document_person_form" : empty_formset, "count_prefix" : new_form_number,"person_type" : person_type})

def docdeleteperson(request, docrolemapping_id):
    if request.method == "DELETE":
        try:
            docrolemapping = DocRoleMapping.objects.get(docrole_id= docrolemapping_id)
            docrolemapping.role_status = 0
            docrolemapping.save()
            return HttpResponse(status=200)
        except:
            print("Error deleting Document Role Mapping")

        return HttpResponse(status=404)
    return HttpResponse(status=400)

###############################################################################################

#### For Pages and References ################################################################


def doceditpagesreferences(request, doc_id):
    document_pages_references_form = None
    if request.method == "POST":
        document_pages_references_form = DocumentPagesReferencesForm(
            request.POST)
        if document_pages_references_form.is_valid():
            document = Document.objects.get(doc_id=doc_id)
            document.doc_num_page = document_pages_references_form.cleaned_data['doc_num_page']
            document.num_references = document_pages_references_form.cleaned_data[
                'num_references']
            document.save()

            return HttpResponse(status=204, headers={'HX-Trigger': HX_TRIGGERS['pagesreferences']})
    else:
        document = Document.objects.get(doc_id=doc_id)
        document_pages_references_form = DocumentPagesReferencesForm(
            instance=document)
    return render(request, "docapp/doc_details_page_templates/doc_pages_references_modal_form.html", {"doc_id": doc_id, "document_pages_references_form": document_pages_references_form})


def docgetpagesreferences(request, doc_id):
    document = Document.objects.get(doc_id=doc_id)
    return render(request, "docapp/doc_details_page_templates/doc_pages_references.html", {"doc_id": doc_id, "doc": document})

###############################################################################################

### Generating Document Number logic ##################################


def get_people_count(doc_id, role_code):
    return DocRoleMapping.objects.filter(role__doc_role_short_code=role_code,doc__doc_id=doc_id,role_status=1).count()

def docgeneratenumber(request,doc_id):
    STARTING_SEQUENCE_NUMBER  = 1000
    document = Document.objects.get(doc_id = doc_id)
    document_number = DocumentNumber.objects.filter(doc__doc_id=doc_id).order_by('-date_created').first()
    current_year = timezone.now().year
    error_messages = []

    for role in [DOC_AUTHOR_CONSTANT, DOC_REVIEWER_CONSTANT,DOC_APPROVER_CONSTANT]:
        people_count = get_people_count(doc_id=doc_id, role_code= role)
        if(people_count == 0):
            error_messages.append("Your document should have atleast one author, approver and reviewer")

    if(document_number.doc_project == None or document_number.doc_type == None):
        error_messages.append("You must select the Entity/Project and the Report Type before generating the document number")

    if(len(error_messages) > 0):
        for error_message in error_messages:
            messages.add_message(request,messages.ERROR,message=error_message)
        return redirect("docapp:docdetails",doc_id=doc_id)    
    
    try:
        docnumber_sequence = DocNumberSequence.objects.get(
                                doc_project = document_number.doc_project,
                                doc_type = document_number.doc_type,
                                doc_year = current_year,
                                doc_sequence_active = 1
                            )
    except:
        docnumber_sequence = None
    
    if(docnumber_sequence is None):
        docnumber_sequence = DocNumberSequence(
                        doc_project = document_number.doc_project,
                        doc_type = document_number.doc_type,
                        doc_year = current_year,
                        doc_sequence_active = 1,
                        doc_sequence_number = STARTING_SEQUENCE_NUMBER
            )
        docnumber_sequence.save()
    
    document_number.document_sequence_number = docnumber_sequence.doc_sequence_number
    
    # Assign the status doc number temporarily assigned 
    document_number.document_number_status = DocumentNumberStatus.objects.get(doc_num_status_name=DOC_NUMBER_STATUSES[1][0])
    document_number.docnumber_text = document_number.document_number
    document_number.save()

    docnumber_sequence.doc_sequence_number = docnumber_sequence.doc_sequence_number + 1
    docnumber_sequence.save()


    document.doc_status = DocumentStatus.objects.get(doc_status_name=DOC_PENDING_UPLOAD)
    document.save()

    messages.add_message(request,messages.SUCCESS, "Document Number Generated :" + document_number.document_number)
    return redirect("docapp:docdetails",doc_id=doc_id)

######################################################################

# Uploading a document
def docupload(request,doc_id):

    # Only documents with statuses DOC_PENDING_UPLOAD and DOC_REUPLOAD must be allowed to submit files
    document = Document.objects.filter(doc_id = doc_id)[0]
    if(document.doc_status != None):
        doc_status = document.doc_status
        if (not(doc_status.doc_status_name == DOC_PENDING_UPLOAD or doc_status.doc_status_name == DOC_REUPLOAD)):
            return redirect("docapp:error")
    else:
        return redirect("docapp:error")

    if request.method == "POST":
        form = DocumentUploadForm(request.POST, request.FILES)
        print(request.FILES)
        print("REACHED FILE UPLOAD POST")
        if form.is_valid():
            file_path = handle_file_upload(doc_id=doc_id,file=request.FILES["document_field"])
            document = Document.objects.get(doc_id = doc_id)
            document.doc_file = file_path[0]
            document.doc_first_page_image = file_path[1]
            document.save()
            print("GOT FILES FROM REQUEST")
        else:
            print(form.errors)
    return redirect("docapp:docdetails",doc_id = doc_id)

#######################################################################

def viewdocument(request,doc_id):
    try:
        document = Document.objects.get(doc_id = doc_id)
        file_content = None
        file_name = None
        if document.doc_file:
           file_path = document.doc_file.name
           file_name = os.path.basename(os.path.normpath(document.doc_file.name))
           with open(file_path,"rb") as file:
            file_content = file.read()
            response = HttpResponse(file_content, content_type="application/pdf")
            response['Content-Disposition'] = 'inline; filename=' + file_name
            return response 
    except:
        return redirect("docapp:error")
    return redirect("docapp:error")

#######################################################################

def viewpdfthumbnail(request,doc_id):
    try:
        document = Document.objects.get(doc_id = doc_id)
        file_content = None
        file_name = None
        if document.doc_first_page_image:
            print("Reached here")
            image_path = os.path.basename(os.path.normpath(document.doc_first_page_image.name))
            image_path = os.path.join(PDF_THUMBNAIL_LOCATION,image_path)

            with open(image_path,"rb")  as file:
                file_content = file.read()
            file_name = str(doc_id)
        else:
            with open(settings.DEFAULT_DOCUMENT_FIRST_IMAGE,"rb") as file:
                file_content = file.read()
            file_name = "error"

        response = HttpResponse(file_content, content_type="image/png")
        response['Content-Disposition'] = 'inline; filename=' + file_name
        return response
    except:
        print("Image does not exist or could not be loaded")
    with open(settings.DEFAULT_DOCUMENT_FIRST_IMAGE,"rb") as file:
        file_content = file.read()
        file_name = "error"
    response = HttpResponse(file_content, content_type="image/png")
    response['Content-Disposition'] = 'inline; filename=' + file_name
    return response
#######################################################################

def submit_to_library(request, doc_id):
    document = Document.objects.get(doc_id = doc_id)
    if(not(document.doc_status.doc_status_name == DOC_PENDING_UPLOAD or document.doc_status.doc_status_name == DOC_REUPLOAD)):
        return redirect("docapp:docdetails",doc_id=doc_id)    
    
    error_messages = []
    if(not document.doc_file):
        error_messages.append("Please upload the technical document before submitting for review")
    if(document.doc_num_page <= 0):
        error_messages.append("The number of pages in a document cannot be zero  less")
    if(len(document.abstract) <=0):
        error_messages.append("Abstract cannot be left blank")
    if len(error_messages) > 0:
        for e in error_messages:
            messages.add_message(request,messages.ERROR,e)
        return redirect("docapp:docdetails",doc_id=doc_id)  
    try:
        with transaction.atomic():
            document_under_review_status = DocumentStatus.objects.get(doc_status_name = DOC_UNDER_REVIEW)
            document.doc_status = document_under_review_status
            document.save()
    except Exception as e:
        print(e)

    return redirect("docapp:docdetails",doc_id=doc_id)
#######################################################################
def dateextension(request, doc_id):
    try:
        document = Document.objects.get(doc_id=doc_id)
        ## doc status must be either reupload or pending upload
        if(not(document.doc_status.doc_status_name == DOC_PENDING_UPLOAD or document.doc_status.doc_status_name == DOC_REUPLOAD)):
            return redirect("docapp:docdetails",doc_id=doc_id)
        if(document.doc_num_extensions_asked >= LIMIT_EXTENSIONS):
            messages.add_message(request,messages.ERROR,"You have reached the maximum number of extensions allowed for this document")
            return redirect("docapp:docdetails",doc_id=doc_id)
        
        document.doc_num_extensions_asked = document.doc_num_extensions_asked + 1
        document.edc = document.edc + timezone.timedelta(days=NUM_EXTENSIONS_DAYS)
        document.save()
        messages.add_message(request,messages.SUCCESS,"Extension requested successfully")
        return redirect("docapp:docdetails",doc_id=doc_id)
    except Exception as e:
        print(e)
        return render(request, "docapp/error_page.html", {})
    
#######################################################################

def docwithdraw(request,doc_id):
    try:
        document = Document.objects.get(doc_id = doc_id)
        documentnumber = DocumentNumber.objects.filter(doc__doc_id = doc_id).last()
        if(document.doc_status.doc_status_name == DOC_NUMBER_WITHDRAWN):
            messages.add_message(request,messages.ERROR,"Document has already been withdrawn")
            return redirect("docapp:docdetails",doc_id=doc_id)
        with transaction.atomic():
            document.doc_status = DocumentStatus.objects.get(doc_status_name = DOC_NUMBER_WITHDRAWN)
            document.save()
            documentnumber.document_number_status = DocumentNumberStatus.objects.get(doc_num_status_name=DOC_NUMBER_STATUSES[2][0])
            documentnumber.save()
            messages.add_message(request,messages.SUCCESS,"Document withdrawn successfully")
        return redirect("docapp:docdetails",doc_id=doc_id)
    except Exception as e:
        print(e)
        return render(request, "docapp/error_page.html", {})
    return render(request, "docapp/error_page.html", {})
#######################################################################

def docsettings(request):
    return render(request, "docapp/settings.html", {})

def error(request):
    return render(request,"docapp/error_page.html",{})

def test(request):
    print(request.POST)
    return HttpResponse("Test Page")
