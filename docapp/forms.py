from typing import Any
from django import forms
from django.forms import BaseFormSet, ModelForm
from .models import Document, Keywords, DocumentNumber
from .models import Employee
from django.forms import BaseFormSet
from docapp.constants import DOCUMENT_STATUSES
from django.core.exceptions import ValidationError


class ModelFormModified(ModelForm):
    
    def is_valid(self):
        result = super().is_valid()
        for x in (self.fields if '__all__' in self.errors else self.errors):
             attrs = self.fields[x].widget.attrs
             attrs.update({'class': attrs.get('class', '') + ' is-invalid'})
        return result

class DocumentCreationForm(ModelFormModified):

    class Meta:
        model = Document
        fields = ['doc_title','control_status','security_classification', 'doc_type_general']
        widgets = {
            'doc_title' : forms.TextInput(attrs={'class':'form-control' , 'aria-Describedby':'titleHelp'}),
            'control_status' : forms.Select(attrs={'class':'form-select'}),
            'security_classification' : forms.Select(attrs={'class':'form-select'}),
            'doc_type_general' : forms.Select(attrs={'class':'form-select'})
        }

class DocumentTitleAbstractForm(ModelFormModified):
    
    error_css_class = "is_invalid"
    
    class Meta:
        model = Document
        fields = ['doc_title','abstract','control_status','security_classification']  
        widgets = {
            'doc_title' : forms.TextInput(attrs={'class':'form-control' , 'aria-Describedby':'titleHelp'}),
            'abstract' : forms.Textarea(attrs={'class':'form-control'}),
            'control_status' : forms.Select(attrs={'class':'form-select'}),
            'security_classification' : forms.Select(attrs={'class':'form-select'}),
        }  

class DocumentKeywordsForm(forms.Form):
    keywords = forms.ModelMultipleChoiceField(
        queryset= Keywords.objects.all()
    )

class DocumentEntityReportTypeForm(ModelFormModified):
        class Meta:
            model = DocumentNumber
            fields = ['doc_project','doc_type']
            widgets = {
                'doc_project' : forms.Select(attrs={'class':'form-select mb-3'}),
                'doc_type' : forms.Select(attrs={'class':'form-select mb-3'}),
            }
        def clean(self):
            cleaned_date = super().clean()
            doc_project = cleaned_date.get('doc_project')
            doc_type = cleaned_date.get('doc_type')
            if doc_project == None:
                self.add_error('doc_project', "A project must be selected")
            if doc_type == None:
                self.add_error('doc_type', "A report type must be selected")
            if doc_project == None or doc_type == None:
                raise forms.ValidationError("Both Entity/Project and Report Type must be selected")

class DocumentPagesReferencesForm(ModelFormModified):
    class Meta:
        model = Document
        fields = ['doc_num_page','num_references']
        widgets = {
            'doc_num_page' : forms.NumberInput(attrs={'class':'form-control'}),
            'num_references' : forms.NumberInput(attrs={'class':'form-control'}),
        }

#################### Form which is used for searching documents ############################

class DocumentAdvancedSearch(forms.Form):
    authors_name = forms.CharField(max_length=100, required=False, label=False,
                                    widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Authors Name'}))
    reviewers_name = forms.CharField(max_length=100, required=False, label=False,
                                     widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Reviewers Name'}))
    approvers_name = forms.CharField(max_length=100, required=False, label=False,
                                      widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Approvers Name'}))
    document_status = forms.ChoiceField(choices = [("ALL","All")] + DOCUMENT_STATUSES, required=False, label=False,
                                        widget=forms.Select(attrs={'class':'form-select'}))
    document_title = forms.CharField(max_length=100, required=False, label=False,
                                        widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Document Title'}))
    
class DocumentNumberSearch(forms.Form):
    document_number = forms.CharField(max_length=100, required=False, label=False,
                                        widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Search by fields in the Doc. No.'}))
    
class DocumentPersonForm(forms.Form):
    person = forms.ModelChoiceField(queryset=Employee.objects.filter(emp_active=1),empty_label="Select Employee", required=True, label=False,
                widget= forms.Select(attrs={'class':'form-select'}))
    print_order = forms.IntegerField(initial=1,widget=forms.NumberInput(attrs={'class':'form-control'}))
    doc_role_mapping_id = forms.IntegerField(disabled=True,required=False)


class DocumentPersonBaseFormSet(BaseFormSet):

    def clean(self):
       super().clean()
       valid_forms_count = 0
       emp_codes = set()

       error_string_prefix = self.prefix.capitalize() + " Form:"
       for form in self.forms:
           if (self.can_delete and self._should_delete_form(form)):
               continue
           person = None
           if 'person' in form.cleaned_data:
               person = form.cleaned_data['person']
               if person is not None:
                if person.emp_code in emp_codes:
                    raise ValidationError(error_string_prefix + "You cannot add the same person twice")
                emp_codes.add(person.emp_code)
           if (person is None):
               print("Reached validation error checking portion")
               raise ValidationError(error_string_prefix + "You cannot leave the person field blank")
           valid_forms_count += 1

       if(valid_forms_count <= 0):
           raise ValidationError(error_string_prefix + "You must enter atleast one valid person")
        

class DocumentUploadForm(forms.Form):
    document_field = forms.FileField(allow_empty_file=True, label="Upload Document", widget=forms.FileInput(attrs={'class' : 'form-control','accept':'application/pdf'}))